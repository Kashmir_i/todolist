import * as angular from 'angular';
import * as deferredBootstrapper from 'angular-deferred-bootstrap';
import { TodoModule }  from './todo/todo.module'
import { TodoConfig }  from './todo/config/todo.config'

const initInjector = angular.injector(["ng"]);
const $http = initInjector.get("$http");

// @ts-ignore
const isCordova: boolean = !!window.cordova;
// @ts-ignore
const cDevice: object = isCordova? device: {
  platform: navigator.platform,
  uuid: '1q2w3e4r5t6y',
  version: navigator.appVersion,
  manufacturer: navigator.appName,
  isVirtual: false,
  serial: navigator.productSub,
  notification_token: 'eq2mKYeTNJk:APA91bFQ6-ir0qzLQnD148LmP4M4DC6N1GyV6VOQiSVHkrg5X72s6SkUBzUD4Ym_84eo2lX_CWWry-R-4md0MFCnnT4-S39Wl7OBZ_A-T_QfncJeKlJ6kaZIBMO4UK-fSLOf-gum5Bj8'
};

const bootstrapApp = (device):void => {
  deferredBootstrapper.bootstrap({
    element: document.body,
    module: TodoModule['module'].name,
    resolve: {
      DEVICE: ['$http', function ($http) {
        return $http.post(TodoConfig.config().urls.device, device);
      }]
    }
  });
};

if (isCordova){
  document.addEventListener("deviceready", () => {
    bootstrapApp(cDevice);
  }, false);
}else {
  bootstrapApp(cDevice);
}
