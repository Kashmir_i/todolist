import {Component, Input} from 'angular-ts-decorators';
import { TodoService } from '../../services/todo.service';
import { TodoInterface } from "../../interfaces/todo.interface";
import * as $ from 'jquery';//TODO: replace jquery bootstrap to angular-bootstrap-ui

@Component({
  selector: 'todo-notification-btn',
  template: require('./todo-notification-btn.template.html'),
})
export class TodoNotificationBtnComponent {

  @Input() private task: TodoInterface;
  @Input() private showInfo: boolean;

  private readonly filter;
  private todoService;
  private datePickerModal;

  /*@ngInject*/
  constructor(todoService: TodoService, $filter) {
    this.filter = $filter;
    this.todoService = todoService;
  }

  public onTimeSet(newDate): void {
    let currentDate = new Date();
    if (new Date(newDate) <= currentDate) {
      alert('You cannot select a past date');
      return;
    }

    this.task.notification_time = this.filter('date')(newDate, 'medium');
    this.todoService.scheduleTask(this.task, () => {
      this.closeModal();
    });
  }

  private closeModal() {
    let { _id } = this.task;
    this.datePickerModal = $(document).find('#datetimepickerModal'+_id);
    this.datePickerModal.modal('hide');
    $(document).find('.modal-backdrop').remove();
    $(document.body).removeClass("modal-open");
  }

  public showNotificationDate(): boolean {
    let currentDate = new Date();
    return (this.showInfo && this.task.notification_time && ( new Date(this.task.notification_time) > currentDate ));
  }
}
