import { Component , Input} from 'angular-ts-decorators';
import { TodoService } from '../../services/todo.service';
import * as $ from 'jquery';//TODO: replace jquery bootstrap to angular-bootstrap-ui
import { TodoInterface } from '../../interfaces/todo.interface';

@Component({
  'selector': 'todo-item',
  template: require('./todo-item.template.html')+
            require('./confirm-modal.template.html'),
})

export class TodoItemComponent {
  @Input() public task: TodoInterface;

  private todoService;
  private confirmModal;
  private readonly filter;

  /*@ngInject*/
  constructor(todoService: TodoService, $filter) {
    this.todoService = todoService;
    this.filter = $filter;
  }

  ngOnInit(): void {
    let { _id } = this.task;
    this.confirmModal = $(document).find('#confirmModal'+_id);
  }

  public confirmAction(): void {
    let { _id } = this.task;
    this.todoService.removeTask(_id, () => {
      this.confirmModal.modal('hide');
      $(document).find('.modal-backdrop').remove();
      $(document.body).removeClass("modal-open");
    });
  };

  public showNotificationDate(): boolean {
    let currentDate = new Date();
    return (this.task.notification_time && ( new Date(this.task.notification_time) > currentDate ));
  }
}
