import { Component } from 'angular-ts-decorators';
import { TodoService } from '../../services/todo.service';
import {TodoInterface} from "../../interfaces/todo.interface";

@Component({
  selector: 'todo-item-view',
  template: require('./todo-item-view.template.html'),
})
export class TodoItemViewComponent {

  private todoService;
  private task: TodoInterface;
  private readonly filter;
  private readonly sce;

  public edit: object = {
    'title': false,
    'description': false
  };
  public submitted: boolean = false;

  /*@ngInject*/
  constructor(todoService: TodoService, $filter, $state, $sce) {
    this.filter = $filter;
    this.sce = $sce;
    this.todoService = todoService;
    this.todoService.getTaskById($state.params.id).then((resp) => {
      this.task = resp[0]
    });
  }

  public showEditField(field: string): void {
    this.edit[field] = !this.edit[field];
  };

  public showDescription() {
    let description  = this.task && this.task.description ? this.task.description :'Click to add description';
    return this.sce.trustAsHtml(description.replace(/\n/g, "<br />"));
  }

  public update(isValid: boolean): void {
    this.submitted = true;
    if (isValid) {
      this.todoService.updateTask({
        "_id": this.task._id,
        "title": this.task.title,
        "description": this.task.description,
        "status": this.task.status,
        "updated_at": this.filter('date')(new Date(), 'medium'),
        "notification_time": this.task.status ? null : this.task.notification_time,
      });

      this.edit = {
        'title': false,
        'description': false
      };
    }
  }
}
