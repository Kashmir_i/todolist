import { Component} from 'angular-ts-decorators';

@Component({
  'selector': 'todo-navbar',
  template: require('./todo-navbar.template.html'),
})

export class TodoNavbarComponent {

  /*@ngInject*/
  constructor() {
  }
}
