import { Component } from 'angular-ts-decorators';
import { TodoService } from "../../services/todo.service";
import { DeviceService } from "../../services/device.service";
import { TodoConfig } from "../../config/todo.config";
import * as $ from 'jquery';

@Component({
  selector: 'todo-create',
  template: require('./todo-create.template.html'),
})
export class TodoCreateComponent {

  private todoService;
  private deviceService;
  private readonly filter;
  private readonly state;
  private createModal;

  public submitted: boolean = false;
  public title: string = '';

  /*@ngInject*/
  constructor(todoService: TodoService, deviceService: DeviceService, $filter, $state) {
    this.todoService = todoService;
    this.filter = $filter;
    this.state = $state;
    this.createModal = $(document).find('#createTodoModal');
    this.deviceService = deviceService;
  }

  public addTodo(isValid: boolean): void {
    this.submitted = true;

    if (isValid) {
      let { _id } = this.deviceService.getDevice();
      this.todoService.setTask({
        "title": this.title,
        "created_at": this.filter('date')(new Date(), 'medium'),
        "device": _id
      },_id);

      this.title = '';
      this.submitted = false;
      this.createModal.modal('hide');
      this.state.go(TodoConfig.config().states.HOME);
    }
  }
}
