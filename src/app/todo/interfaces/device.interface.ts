export interface DeviceInterface {
  _id: string;
  uuid: string;
  platform?: string;
  version?: string;
  manufacturer?: string;
  isVirtual?: boolean;
  serial?: string;
  notification_token?: string;
  tasks?: object;
}