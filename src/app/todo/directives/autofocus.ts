import { Directive } from 'angular-ts-decorators';

@Directive({
  selector: 'auto-focus',
  restrict: 'A'
})
export class Autofocus {

  private readonly scope;
  private readonly element;
  private readonly timeout;

  /*@ngInject*/
  constructor($scope, $element, $timeout) {
    this.scope = $scope;
    this.element = $element;
    this.timeout = $timeout;
  }

  ngOnInit(): void {
    if (this.element.attr('ng-show')) {
      this.scope.$watch(this.element.attr('ng-show'), (newValue) => {
        if (newValue) {
          this.timeout(() => {
            this.element[0].focus();
          }, 0);
        }
      })
    }
    if (this.element.attr('ng-hide')) {
      this.scope.$watch(this.element.attr('ng-hide'), (newValue) => {
        if (!newValue) {
          this.timeout(() => {
            this.element[0].focus();
          }, 0);
        }
      })
    }
  }
}
