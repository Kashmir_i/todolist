import { Directive } from 'angular-ts-decorators';

@Directive({
  selector: 'http-request-loader',
  restrict: 'E'
})

export class HttpRequestLoader {

  private readonly scope;
  private readonly element;
  private readonly http;

  /*@ngInject*/
  constructor($scope, $element, $http) {
    this.scope = $scope;
    this.element = $element;
    this.http = $http;
  }

  ngOnInit(): void {
    this.scope.isLoading = () => {
      return this.http.pendingRequests.length > 0;
    };
    this.scope.$watch(this.scope.isLoading, (value) => {
      if (value)
        this.element.removeClass('ng-hide');
      else
        this.element.addClass('ng-hide');
    });
  }
}
