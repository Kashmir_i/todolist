import { Directive } from 'angular-ts-decorators';

@Directive({
  selector: 'auto-size',
  restrict: 'A'
})

export class Autosize {

  private readonly scope;
  private readonly element;
  private readonly timeout;

  /*@ngInject*/
  constructor($scope, $element, $timeout) {
    this.scope = $scope;
    this.element = $element;
    this.timeout = $timeout;
    console.log('fdf')
  }

  ngOnInit(): void {
    this.element.css({ 'height': 'auto', 'overflow-y': 'hidden' });
    this.timeout(() => {
      this.element.css('height', (this.element[0].scrollHeight === 0 ? 48 : this.element[0].scrollHeight) + 'px');
    }, 100);

    this.element.on('input focus', () => {
      this.element.css({ 'height': 'auto', 'overflow-y': 'hidden' });
      this.element.css('height', this.element[0].scrollHeight + 'px');
    });
  }
}
