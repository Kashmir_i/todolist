import { Injectable } from 'angular-ts-decorators';
import { TodoConfig } from "../config/todo.config";
import { DeviceInterface } from "../interfaces/device.interface";

@Injectable()
export class DeviceService {

  private http;
  private q;
  private timeout;
  private device: DeviceInterface;

  private URLS: object = {
    FETCH: TodoConfig.config().urls.device
  };

  /*@ngInject*/
  constructor($q, $http, $timeout) {
    this.http = $http;
    this.q = $q;
    this.timeout = $timeout;
  }

  public getDevice(): DeviceInterface {
    // let deferred = this.q.defer();
    // if (this.device._id) {
    //   deferred.resolve(this.device)
    // } else {
    //   this.setDevice(this.device, (resp) => {
    //     deferred.resolve(resp.data);
    //   });
    // }
    // return deferred.promise;
    return this.device;
  };

  public initDevice(device) {
    this.device = device;
  }

  public setDevice(device, callback: (r) => void): void {
    this.http.post(this.URLS['FETCH'], device).then((resp) => {
      this.device = resp.data;
      callback(resp);
    });
  }

  public updateDevice(device: DeviceInterface): void {
    let { _id } = device;
    this.http.put(this.URLS['FETCH']+'/'+_id, device).then((resp) => {
      this.device = resp.data;
    })
  }
}
