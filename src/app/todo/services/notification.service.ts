import { Injectable } from 'angular-ts-decorators';
import { DeviceService } from './device.service';

@Injectable()
export class NotificationService {

  private pushOptions: object = {
    android: { senderID: '821651481512' },
  };

  private push;
  private deviceService;

  /*@ngInject*/
  constructor(deviceService: DeviceService) {
    this.deviceService = deviceService;
  }

  public onDeviceReady(): void {
    console.log('device ready');
    // @ts-ignore
    this.push = PushNotification.init({
      android: {},
    });

    this.push.on('registration', (data): void => {
      console.log("device token: " + data.registrationId);

      let device = this.deviceService.getDevice();
      device.notification_token = data.registrationId;
      this.deviceService.updateDevice(device);

    });
    this.push.on('notification', (data): void => {
      console.log(data.message);
      console.log(data.title);
      console.log(data.count);
      console.log(data.sound);
      console.log(data.image);
      console.log(data.additionalData);
    });
    this.push.on('error', err => {
      console.log(err.message)
    });
  };
}
