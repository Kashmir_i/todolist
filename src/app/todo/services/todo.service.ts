import { Injectable } from 'angular-ts-decorators';
import { TodoConfig } from "../config/todo.config";
import * as _ from 'lodash';

@Injectable()
export class TodoService {

  private http;
  private q;
  private timeout;

  private URLS: object = {
    TASKS: TodoConfig.config().urls.tasks,
    DEVICE: TodoConfig.config().urls.device,
  };

  private tasks;

  /*@ngInject*/
  constructor($q, $http, $timeout) {
    this.http = $http;
    this.q = $q;
    this.timeout = $timeout;
  }

  private cacheTasks(data): object {
    this.tasks = data.data;
    return this.tasks;
  }
  private findTask(taskId: string): object {
    return this.tasks.filter((task) => task._id === taskId)
  }
  private updateCached(_id, updated) {
    let index = _.findIndex(this.tasks, function (t) {
      return t._id === _id
    });
    if (this.tasks)
      this.tasks[index] = updated;
  }

  public getTasks(deviceId): void {
    return (this.tasks) ? this.q.when(this.tasks) : this.http.get(this.URLS['DEVICE']+'/'+ deviceId +'/tasks').then(resp => {
      return this.cacheTasks(resp);
    },(e) => {
      if (e.xhrStatus === 'error') {
        // this.timeout(() => {
        //   console.log('trying to get tasks');
        //   return this.getTasks();
        // },8000)
      }
      throw e;
    });
  }

  public getTaskById(taskId: string): void {
    let deferred = this.q.defer();
    if (this.tasks) {
      deferred.resolve(this.findTask(taskId))
    } else {
      this.http.get(this.URLS['TASKS']+'/'+taskId).then((resp) => {
        deferred.resolve(resp.data)
      })
    }
    return deferred.promise;
  };

  public setTask(task: object, deviceId): void {
    this.http.post(this.URLS['DEVICE']+'/'+ deviceId +'/tasks', task).then((resp) => {
      this.tasks.push(resp.data)
    });
  };

  public updateTask(task: object): void {
    this.http.put(this.URLS['TASKS']+'/'+task['_id'], task).then((resp) => {
      this.updateCached(task['_id'],resp.data);
    })
  };

  public removeTask(id: string, callback: () => void): void {
    this.http.delete(this.URLS['TASKS']+'/'+id).then((resp) => {
      if (resp.data.message && resp.data.message === 'ok') {
        _.remove(this.tasks, function (task) {
          return task._id === id;
        });
        callback();
      }else {
        console.log(resp.data.message)
      }
    });
  };

  public scheduleTask(task: object, callback: () => void): void {
    this.http.put(this.URLS['TASKS']+'/'+task['_id']+'/notification', task).then((resp) => {
      this.updateCached(task['_id'],resp.data);
      callback();
    })
  };
}
