import { Component } from 'angular-ts-decorators';
import { TodoService } from '../../services/todo.service';
import { DeviceService } from "../../services/device.service";
import { DeviceInterface } from "../../interfaces/device.interface";
import {TodoInterface} from "../../interfaces/todo.interface";
import {debug} from "webpack";

//TODO: replace jquery bootstrap to angular-bootstrap-ui

@Component({
  selector: 'todo-list',
  template: require('./todo-list.template.html'),
})
export class TodoListComponent {
  private todoService;
  private deviceService;
  private device: DeviceInterface;
  private readonly timeout;

  public todos: TodoInterface;
  public orderProp: string = 'created_at';
  public reverseOrder: boolean = true;
  public loading: boolean = true;

  /*@ngInject*/
  constructor(todoService: TodoService, deviceService: DeviceService, $timeout) {
    this.todoService = todoService;
    this.deviceService = deviceService;
    this.timeout = $timeout;
  }

  ngOnInit(): void {
    this.device = this.deviceService.getDevice();
    this.loadTasks();
  }

  private loadTasks(): void {
    let { _id } = this.device;
    this.todoService.getTasks(_id).then(data => {
      this.todos = data;
      this.loading = false;
    })
  }

  public orderProps: object = {
    'created_at': 'Date',
    'title': 'Title',
    'status': 'Status',
  };

  public orderBy(orderProp: string): void {
    this.reverseOrder = (this.orderProp === orderProp) ? !this.reverseOrder : false;
    this.orderProp = orderProp;
  }
}
