import { Component } from 'angular-ts-decorators';

@Component({
  selector: 'todo-detail',
  template: require('./todo-detail.template.html'),
})
export class TodoDetailComponent {
  /*@ngInject*/
  constructor() {}
}
