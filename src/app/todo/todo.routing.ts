import {NgModule} from 'angular-ts-decorators';
import { StateProvider } from '@uirouter/angularjs';
import {TodoConfig} from './config/todo.config';

@NgModule({
  id: 'TodoRouting'
})

export class TodoRouting {
  static config($stateProvider: StateProvider) {
    'ngInject';
    $stateProvider
      .state({
        name: TodoConfig.config().states.HOME,
        url: '',
        redirectTo: TodoConfig.config().states.TODO_LIST
      })
      .state({
        name: TodoConfig.config().states.TODO_LIST,
        url: '/todos',
        views: {
          content: {
            component:'todoList'
          }
        }
      })
      .state({
        name: TodoConfig.config().states.TODO_VIEW,
        url: '/todo/{id}/detail',
        views: {
          content: {
            component: 'todoDetail'
          }
        }
      })
  }
}