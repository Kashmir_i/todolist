import { NgModule } from 'angular-ts-decorators';

@NgModule({
  id: 'TodoConfig',
})
export class TodoConfig {
  static config() {
    return {
      urls: {
        // tasks: 'http://127.0.0.1:4521/api/tasks',
        // device: 'http://127.0.0.1:4521/api/device',
        tasks: 'http://198.8.86.87:4521/api/tasks',
        device: 'http://198.8.86.87:4521/api/device',
      },
      states: {
        HOME: 'home',
        TODO_LIST: 'todo-list',
        TODO_VIEW: 'todo-view',
      }
    };
  }
}