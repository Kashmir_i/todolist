import { NgModule } from 'angular-ts-decorators';
import '@uirouter/angularjs';
import { TodoRouting } from './todo.routing';
import 'angular-resource';

import { TodoNavbarComponent } from "./components/navbar/todo-navbar.component";
import { TodoItemComponent } from "./components/item/todo-item.component";
import { TodoListComponent } from './pages/list/todo-list.component';
import { TodoItemViewComponent } from './components/item-view/todo-item-view.component';
import { TodoDetailComponent } from './pages/detail/todo-detail.component';
import { TodoNotificationBtnComponent } from "./components/notification-btn/todo-notification-btn.component";
import { TodoCreateComponent } from './components/create/todo-create.component';

import { NotificationService } from "./services/notification.service";
import { HttpRequestLoader } from "./directives/httpRequestLoader";
import { DeviceService } from "./services/device.service";
import { TodoService } from "./services/todo.service";
import { TodoConfig } from "./config/todo.config";
import { Autofocus } from "./directives/autofocus";
import { Autosize } from "./directives/autosize";


import 'bootstrap/scss/bootstrap.scss';
import 'angularjs-bootstrap-datetimepicker/src/css/datetimepicker.css';
import 'jquery';
import 'bootstrap/js/dist/modal';
import 'popper.js';
import 'bootstrap/js/dist/dropdown';
import 'angularjs-bootstrap-datetimepicker';
import '../../assets/css/app.scss';
import '../../assets/css/icomoon.css';

// @ts-ignore
const isCordova: boolean = !!window.cordova;

@NgModule({
  id: 'TodoModule',
  imports: [
    'ui.router',
    TodoRouting,
    'ui.bootstrap.datetimepicker',
  ],
  declarations: [
    TodoNavbarComponent,
    TodoItemComponent,
    TodoListComponent,
    TodoItemViewComponent,
    TodoDetailComponent,
    TodoCreateComponent,
    TodoNotificationBtnComponent,
    Autofocus,
    Autosize,
    HttpRequestLoader
  ],
  providers: [
    { provide: 'NotificationService', useClass: NotificationService },
    { provide: 'deviceService', useClass: DeviceService },
    { provide: 'todoService', useClass: TodoService },
    { provide: 'TodoConfig', useValue: TodoConfig.config() }
  ]
})

export class TodoModule{

  /*@ngInject*/
  static run(NotificationService, deviceService, DEVICE): void {
    deviceService.initDevice(DEVICE);
    if (isCordova){
      document.addEventListener("deviceready", () => {
        NotificationService.onDeviceReady()
      }, false);
    }
  }
}
